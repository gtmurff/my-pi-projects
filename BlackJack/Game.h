//
//  Game.h
//  BlackJack
//
//  Created by charlie on 6/10/12.
//  Copyright (c) 2012 MurffWare Technologies, LLC. All rights reserved.
//

#ifndef BlackJack_Game_h
#define BlackJack_Game_h

void Deal(int players);
void printHands(int players);
void takeTurns(int players);
void playHand(int player);
int getHandValue(int player);
void hit(int player);

#endif
