//
//  Deck.h
//  BlackJack
//
//  Created by charlie on 6/5/12.
//  Copyright (c) 2012 MurffWare Technologies, LLC. All rights reserved.
//

#ifndef BlackJack_Deck_h
#define BlackJack_Deck_h

int bicycle[52];

int shuffle();
void initializeDeck();
void printDeck();
int dealTopCard();

#endif
