//
//  Game.c
//  BlackJack
//
//  Created by charlie on 6/10/12.
//  Copyright (c) 2012 MurffWare Technologies, LLC. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "Deck.h"
#include "Card.h"
#include "Game.h"
#include <string.h>

int playerHands[10][10];

void Deal(int players)
{
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < players + 1; j++)
        {
            if (i < 2){
                playerHands[j][i] = dealTopCard();
            }
            else {
                playerHands[j][i] = 999;
            }
        }
    }
}

void printHands(int players)
{
    for (int i = 0; i < players + 1; i++)
    {
        if (i < players){
            printf("Player %d: [%s][%s]\n", i, cardName(playerHands[i][0]), cardName(playerHands[i][1]));
        }
        else{
            printf("Dealer: [%s][%s]\n", cardName(playerHands[i][0]), cardName(playerHands[i][1]));
        }
            
    }
}

void takeTurns(int players)
{
    for (int i = 0; i < players; i++)
    {
        playHand(i);
    }
    playHand(999);
}

void playHand(int player)
{
    int handValue = 0;
    //int isDealer = 0;
    handValue = getHandValue(player);
    printf("Player %d current value: %d\n", player, handValue);
    while (handValue < 17)
    {
        hit(player);
        handValue = getHandValue(player);
        printf("Player %d current value: %d\n", player, handValue);
    }
    
    printf("\n");
}

int getHandValue(int player)
{
    int toReturn = 0;
    int count = 0;
    int aceIsEleven = 0;
    char *cards;
    
    cards = malloc(10*sizeof(char));
    
    while (playerHands[player][count] != 999){
        cards[count] = cardName(playerHands[player][count])[0];
        
        switch (cards[count]) {
            case 'A':
                toReturn += 11;
                aceIsEleven += 1;
                if (toReturn > 21){
                    toReturn -= 10;
                    aceIsEleven -= 1;
                }
                break;
            case 'J':
                toReturn += 10;
                break;
            case 'Q':
                toReturn += 10;
                break;
            case 'K':
                toReturn += 10;
                break;
            case '1':
                toReturn += 10;
                break;
                
            default:
                toReturn += atoi(&cards[count]);
                break;
        }
        
        count++;
        
    }
    if (toReturn > 21){
        while (aceIsEleven > 0){
            toReturn -= 10;
            aceIsEleven -= 1;
        }
    }
    return toReturn;
    
}

void hit(int player)
{
    int numberOfCards = 0;
    while (playerHands[player][numberOfCards] != 999){
        numberOfCards++;
    }
    playerHands[player][numberOfCards] = dealTopCard();
    printf("Dealt: %s\n", cardName(playerHands[player][numberOfCards]));
        
}




