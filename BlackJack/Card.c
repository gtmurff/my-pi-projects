//
//  Card.c
//  BlackJack
//
//  Created by charlie on 6/10/12.
//  Copyright (c) 2012 MurffWare Technologies, LLC. All rights reserved.
//

#include <stdio.h>
#include "Card.h"
#include <string.h>
#include <stdlib.h>

char *cardName(int cardNumber)
{
    char *cardNameToPrint;
    int toTranslate = 999;
    if (cardNumber < 52){
        toTranslate = cardNumber;
    }
    else if (cardNumber < 104){
        toTranslate = cardNumber - 52;
    }
    else if (cardNumber < 156){
        toTranslate = cardNumber - 104;
    }
    else if (cardNumber < 208){
        toTranslate = cardNumber - 156;
    }
    else if (cardNumber < 260){
        toTranslate = cardNumber - 208;
    }
    else if (cardNumber < 312){
        toTranslate = cardNumber - 260;
    }
    
    cardNameToPrint = (char *)malloc(sizeof(char)*3);
    
    switch (toTranslate % 13) {
        case 0:
            cardNameToPrint = strcat(cardNameToPrint, "A");
            break;
        case 1:
            cardNameToPrint = strcat(cardNameToPrint, "2");
            break;
        case 2:
            cardNameToPrint = strcat(cardNameToPrint, "3");
            break;
        case 3:
            cardNameToPrint = strcat(cardNameToPrint, "4");
            break;
        case 4:
            cardNameToPrint = strcat(cardNameToPrint, "5");
            break;
        case 5:
            cardNameToPrint = strcat(cardNameToPrint, "6");
            break;
        case 6:
            cardNameToPrint = strcat(cardNameToPrint, "7");
            break;
        case 7:
            cardNameToPrint = strcat(cardNameToPrint, "8");
            break;
        case 8:
            cardNameToPrint = strcat(cardNameToPrint, "9");
            break;
        case 9:
            cardNameToPrint = strcat(cardNameToPrint, "10");
            break;
        case 10:
            cardNameToPrint = strcat(cardNameToPrint, "J");
            break;
        case 11:
            cardNameToPrint = strcat(cardNameToPrint, "Q");
            break;
        case 12:
            cardNameToPrint = strcat(cardNameToPrint, "K");
            break;
            
        default:
                // do nothing
            break;
    }
    
    //cardNameToPrint = realloc(cardNameToPrint, 8*sizeof(char));
    
    switch (toTranslate / 13) {
        case 0:
            strcat(cardNameToPrint, "c");
            break;
        case 1:
            strcat(cardNameToPrint, "h");
            break;
        case 2:
            strcat(cardNameToPrint, "s");
            break;
        case 3:
             strcat(cardNameToPrint, "d");
            break;
            
        default:
            break;
    }
    
    return cardNameToPrint;
}