//
//  main.c
//  BlackJack
//
//  Created by charlie on 6/5/12.
//  Copyright (c) 2012 MurffWare Technologies, LLC. All rights reserved.
//

//  CLUBS, HEARTS, SPADES, DIAMONDS
//  CLUBS, HEARTS, SPADES, DIAMONDS
//  CLUBS, HEARTS, SPADES, DIAMONDS

#include <stdio.h>
#include "Deck.h"
#include "Game.h"



int main(int argc, const char * argv[])
{
    
    // insert code here...
    int Players = 8;
    initializeDeck();
    shuffle(10000);
    Deal(Players);
    printHands(Players);
    takeTurns(Players);
    return 0;
}

